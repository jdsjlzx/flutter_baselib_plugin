///@date:  2021/10/8 11:48
///@author:  lixu
///@description: 分页加载和下拉刷新功能演示
class NewsBean {
  NewsBean({
    this.uniquekey,
    this.title,
    this.date,
    this.category,
    this.authorName,
    this.url,
    this.thumbnailPicS,
    this.thumbnailPicS02,
    this.thumbnailPicS03,
    this.isContent,
  });

  NewsBean.fromJson(Map<String, dynamic> json) {
    uniquekey = json['uniquekey'];
    title = json['title'];
    date = json['date'];
    category = json['category'];
    authorName = json['author_name'];
    url = json['url'];
    thumbnailPicS = json['thumbnail_pic_s'];
    thumbnailPicS02 = json['thumbnail_pic_s02'];
    thumbnailPicS03 = json['thumbnail_pic_s03'];
    isContent = json['is_content'];
  }

  String? uniquekey;
  String? title;
  String? date;
  String? category;
  String? authorName;
  String? url;
  String? thumbnailPicS;
  String? thumbnailPicS02;
  String? thumbnailPicS03;
  String? isContent;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['uniquekey'] = uniquekey;
    map['title'] = title;
    map['date'] = date;
    map['category'] = category;
    map['author_name'] = authorName;
    map['url'] = url;
    map['thumbnail_pic_s'] = thumbnailPicS;
    map['thumbnail_pic_s02'] = thumbnailPicS02;
    map['thumbnail_pic_s03'] = thumbnailPicS03;
    map['is_content'] = isContent;
    return map;
  }
}
