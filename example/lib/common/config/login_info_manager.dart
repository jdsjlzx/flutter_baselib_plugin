import 'package:flutter_baselib_example/module/login/model/user_bean.dart';

///@date:  2021/3/2 11:44
///@author:  lixu
///@description:登录信息管理
final LoginInfoManager loginInfo = LoginInfoManager();

class LoginInfoManager {
  static LoginInfoManager _instance = LoginInfoManager._internal();

  LoginInfoManager._internal();

  factory LoginInfoManager() {
    return _instance;
  }

  ///当期用户信息
  UserBean? userBean;

  ///登录成功，保存的token
  String? token;

  int? getCenterId() {
    if (userBean != null && userBean!.centerIds != null && userBean!.centerIds!.isNotEmpty) {
      return userBean!.centerIds![0];
    } else {
      return null;
    }
  }
}
