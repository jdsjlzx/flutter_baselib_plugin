///@date:  2021/2/26 18:10
///@author:  lixu
///@description:
class HttpConst {
  ///http超时
  static const httpTimeOut = 10 * 1000;

  ///wv接口请求网络成功code
  static const httpResultSuccess = 200;

  ///聚合接口请求成功code
  static const juheHttpResultSuccess = 0;

  ///token错误code
  static const sysTokenError = 900001;

  ///token过期code
  static const sysTokenExpired = 900002;

  static const serverKey = '9vFLsTux4GN5dGFuDMhL';
}
