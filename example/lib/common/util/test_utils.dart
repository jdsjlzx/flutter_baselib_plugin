import 'package:flutter_baselib/flutter_baselib.dart';

///@date:  2021/8/17 13:42
///@author:  lixu
///@description:
class TestUtils {
  ///专门打印控件build方法测试
  static void logBuild(String log) {
    LogUtils.e("TestUtils--ViewBuild--BaseView", log);
  }
}
