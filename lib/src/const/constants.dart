///@date:  2021/3/1 11:44
///@author:  lixu
///@description: 常量类
class Constants {
  ///分页加载其起始页index
  static int initPageIndex = 0;

  ///分页加载页大小
  static int pageSize = 20;

  static const String httpStartWith = 'http';
}
