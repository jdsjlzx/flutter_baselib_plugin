import 'package:shared_preferences/shared_preferences.dart';

///SharedPreferences 工具类
class SpUtils {
  SpUtils._();

  static Future<SharedPreferences> getSharedPreferences() {
    return SharedPreferences.getInstance();
  }

  static Future<bool> containsKey(String key) async {
    return (await getSharedPreferences()).containsKey(key);
  }

  static Future<bool> setString(String key, value) async {
    return (await getSharedPreferences()).setString(key, value.toString());
  }

  static Future<String?> getString(String key) async {
    return (await getSharedPreferences()).getString(key);
  }

  static Future<bool> setInt(String key, int value) async {
    return (await getSharedPreferences()).setInt(key, value);
  }

  static Future<int?> getInt(String key) async {
    return (await getSharedPreferences()).getInt(key);
  }

  static Future<bool> setDouble(String key, double value) async {
    return (await getSharedPreferences()).setDouble(key, value);
  }

  static Future<double?> getDouble(String key) async {
    return (await getSharedPreferences()).getDouble(key);
  }

  static Future<bool> setBool(String key, bool value) async {
    return (await getSharedPreferences()).setBool(key, value);
  }

  static Future<bool?> getBool(String key) async {
    return (await getSharedPreferences()).getBool(key);
  }

  static Future get(String key) async {
    return (await getSharedPreferences()).get(key);
  }

  static Future<bool> remove(String key) async {
    return (await getSharedPreferences()).remove(key);
  }
}
