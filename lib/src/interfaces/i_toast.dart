///@date:  2021/3/10 14:41
///@author:  lixu
///@description: Toast接口
abstract class IToast {
  ///显示Toast
  void show(String? text, {bool isShowLong = false});

  ///仅在debug环境显示的Toast,方便调测用的
  void showDebug(String? text, {bool isShowLong = false});
}
