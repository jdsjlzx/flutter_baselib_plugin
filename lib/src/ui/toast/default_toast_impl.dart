import 'package:flutter/material.dart';
import 'package:flutter_baselib/flutter_baselib.dart';

///@date:  2021/3/10 14:45
///@author:  lixu
///@description: 默认toast实现
class DefaultToastImpl extends IToast {
  static String _tag = 'DefaultToastImpl';
  static bool _isFirst = true;

  @override
  void show(String? text, {bool isShowLong = false}) {
    if (text != null && text.length > 0) {
      if (_isFirst) {
        _isFirst = false;
      } else {
        Fluttertoast.cancel();
      }

      Fluttertoast.showToast(
        msg: text,
        toastLength: isShowLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.black87,
        textColor: Colors.white,
        fontSize: 14,
      );
      LogUtils.i(_tag, '用户可见的Toast Text:$text');
    } else {
      LogUtils.w(_tag, "toast content is empty");
    }
  }

  @override
  void showDebug(String? text, {bool isShowLong = false}) {
    if (BaseLibPlugin.isDebug) {
      if (text != null && text.length > 0) {
        if (_isFirst) {
          _isFirst = false;
        } else {
          Fluttertoast.cancel();
        }

        Fluttertoast.showToast(
          msg: text,
          toastLength: isShowLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16,
        );
      } else {
        LogUtils.w(_tag, "showDebug toast content is empty");
      }
    }
  }
}
